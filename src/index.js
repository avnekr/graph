
import React from 'react';
import { render } from 'react-dom';
import Chart from './Chart';
import { data} from './data';

import { TypeChooser } from "react-stockcharts/lib/helper";

class ChartComponent extends React.Component {
	render() {
		return (
			<TypeChooser>
				{type => <Chart type={type} data={data.map(data => ({
					...data,
					date: new Date(data.date)
				}))} />}
			</TypeChooser>
		)
	}
}

render(
	<ChartComponent />,
	document.getElementById("root")
);
