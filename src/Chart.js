import React from "react";

import { format } from "d3-format";
import { timeFormat } from "d3-time-format";

import {ChartCanvas, Chart} from "react-stockcharts";
import {
	CandlestickSeries,
	BollingerSeries
} from "react-stockcharts/lib/series";
import { XAxis, YAxis } from "react-stockcharts/lib/axes";
import {
	CrossHairCursor,
	MouseCoordinateX,
	MouseCoordinateY,
} from "react-stockcharts/lib/coordinates";

import { discontinuousTimeScaleProvider } from "react-stockcharts/lib/scale";
import { HoverTooltip } from "react-stockcharts/lib/tooltip";
import { fitWidth } from "react-stockcharts/lib/helper";
import { last } from "react-stockcharts/lib/utils";

const dateFormat = timeFormat("%Y-%m-%d");
const numberFormat = format(".2f");

const tooltipContent = ({ currentItem, xAccessor }) => {
	return {
		x: dateFormat(xAccessor(currentItem)),
		y: [
			{
				label: "open",
				value: currentItem.open && numberFormat(currentItem.open)
			},
			{
				label: "high",
				value: currentItem.high && numberFormat(currentItem.high)
			},
			{
				label: "low",
				value: currentItem.low && numberFormat(currentItem.low)
			},
			{
				label: "close",
				value: currentItem.close && numberFormat(currentItem.close)
			}
		]
			.filter(line => line.value)
	};
};

const bbStroke = {
	top: "#964B00",
	middle: "#000000",
	bottom: "#964B00",
};

const bbFill = "#4682B4";

const keyValues = ["high", "low"];

class CandleStickChartWithHoverTooltip extends React.Component {
	removeRandomValues(data) {
		return data.map(item => {
			const newItem = { ...item };
			const numberOfDeletion =
				Math.floor(Math.random() * keyValues.length) + 1;
			for (let i = 0; i < numberOfDeletion; i += 1) {
				const randomKey =
					keyValues[Math.floor(Math.random() * keyValues.length)];
				newItem[randomKey] = undefined;
			}
			return newItem;
		});
	}

	render() {
		let { type, data: initialData, width, ratio } = this.props;

		// remove some of the data to be able to see
		// the tooltip resize
		initialData = this.removeRandomValues(initialData);

		const margin = { left: 80, right: 80, top: 30, bottom: 50 };

		const xScaleProvider = discontinuousTimeScaleProvider.inputDateAccessor(
			d => d.date
		);
		const { data, xScale, xAccessor, displayXAccessor } = xScaleProvider(
			initialData
		);

		const start = xAccessor(last(data));
		const end = xAccessor(data[Math.max(0, data.length - 150)]);
		const xExtents = [start, end];

		return (
			<ChartCanvas
				height={400}
				width={width}
				ratio={ratio}
				margin={margin}
				type={type}
				seriesName="MSFT"
				data={data}
				xScale={xScale}
				xAccessor={xAccessor}
				displayXAccessor={displayXAccessor}
				xExtents={xExtents}
			>
				<Chart
					id={1}
					yExtents={[
						d => [d.high, d.low]
					]}
					padding={{ top: 10, bottom: 20 }}
				>
					<XAxis axisAt="bottom" orient="bottom" />

					<YAxis axisAt="right" orient="right" ticks={5} />

					<CandlestickSeries />

					<BollingerSeries yAccessor={({open, close}) => {
						return {
							top: Math.max(open, close) + 1,
							middle: Math.min(open, close) + (Math.abs(open - close) / 2),
							bottom: Math.min(open, close) - 1
						};
					}}
									 stroke={bbStroke}
									 fill={bbFill} />

					<HoverTooltip
						tooltipContent={tooltipContent}
						fontSize={15}
					/>
					<MouseCoordinateX
						at="bottom"
						orient="bottom"
						displayFormat={timeFormat("%Y-%m-%d")} />
					<MouseCoordinateY
						at="right"
						orient="right"
						displayFormat={format(".2f")} />
				</Chart>
				<CrossHairCursor />
			</ChartCanvas>
		);
	}
}


CandleStickChartWithHoverTooltip.defaultProps = {
	type: "svg"
};
CandleStickChartWithHoverTooltip = fitWidth(CandleStickChartWithHoverTooltip);

export default CandleStickChartWithHoverTooltip;
